# Szenario C

Sie arbeiten bei der IT-Solutions und betreuen eine Vielzahl von Hamburger Unternehmen.

Einer Ihrer Kunden ist an Sie herangetreten und beauftragt Sie mit der Entwicklung eines **Quizspiels** mit dem Namen "BrainBusters". Das Spiel soll vor allem dazu dienen, die Aufmerksamkeit potenzieller Kunden auf sich zu ziehen und die Marke bekannter zu machen sowie die bestehende Kundenbeziehungen zu pflegen und auszubauen. Dem entsprechend erwartet der Kunde, dass sich die **Fragendatenbank** einfach aufbauen und beliebig erweitern lässt.  
Den Spielern von "BrainBuster" soll eine spannende und herausfordernde Quizumgebung geboten werden, in der sie ihr **Wissen in verschiedenen Kategorien** testen können. Um den Spaßfaktor weiter zu erhöhen soll es den Spielern, ähnlich wie bei [Quizduell](<https://de.wikipedia.org/wiki/Quizduell_(App)>), ermöglicht werden, **gegen ihre Freunde oder zufällige Gegner anzutreten**.

IT-Solutions steht für qualitativ hochwertige Software. Das Unternehmen arbeitet seit geraumer Zeit iterativ - umgesetzt mit Hilfe von Continious-Delivery-Pipelines.

Entwicklen Sie einen **Prototyp** des gewünschten Quizspiels. Erstellen Sie unter Berücksichtigung des **Software-Developement-Life-Cycles eine Pipeline**. Erstellen Sie geeignete **(Test-)Szenarien** für Ihre Software und überprüfen diese möglichst mit Hilfe **automatisierter Tests**. Entwickeln Sie die Software und die Pipeline **iterativ**.

Bearbeiten Sie auch das Thema **Datenschutz oder "Open Source vs. Closed Source Software"**.

Beurteilen Sie Ihren (Team-)Prozess und Ihr Produkt. Zeigen Sie Optimierungen und Verbesserungsmöglichkeiten auf und wo ggf. Probleme entstanden sind.

Weitere Schritte zur Vorgehensweise und Leitfragen für die Themen sind im [Szenario A](https://moodle.itech-bs14.de/mod/page/view.php?id=41383) beschrieben. Beachten Sie auch die Beurteilungskriterien!

## Szenario A vs. Szenario C

| Szenario A                                                                   | Szenario C              |
| ---------------------------------------------------------------------------- | ----------------------- |
| anhand von technischen Anforderungen kann die Software frei definiert werden | Quiz Game "BrainBuster" |
| Continous Delivery Pipeline                                                  | identisch zu Szenario A |
| Iterativer Softwareentwicklungszyklus                                        | identisch zu Szenario A |
| Datenschutz oder "Open Source vs. Closed Source Software"                    | identisch zu Szenario A |

## Teil 2: Quiz Game "BrainBuster"

![logo](public/logo.png "BrainBuster")

_Hinweis: Für Einsteiger in die Programmierung empfehlen wir die Programmiersprache Python._

Das Hauptziel von BrainBuster ist es, Spielern die Möglichkeit zu geben, ihr Wissen in verschiedenen Kategorien zu testen und zu erweitern.  
Das Spiel soll sowohl für Gelegenheitsspieler als auch für Quiz-Enthusiasten ansprechend sein und ein breites Spektrum an Quizfragen und Herausforderungen bieten.

### Arbeitsauftrag

Das Spiel soll folgende Basisfunktionen besitzen:

- es stehen verschiedene Kategorien von Quizfragen zur Auswahl
- Quizfragen und weitere Kategorien können über ein entsprechendes Backend angelegt werden
- es gibt ein Punktesystem und Belohnungen für richtig beantwortete Fragen, schnelle Reaktionszeiten und andere Leistungen haben
- Spieler sollten die Möglichkeit haben, entweder alleine gegen die Zeit oder gegen andere Spieler anzutreten
- es sollte eine globale Rangliste geben, in der Spieler ihre Leistungen mit anderen vergleichen können

Ihre Aufgabe besteht darin, nach diesen Vorgaben einen **Prototypen** für dieses Quizgame zu entwickeln.

Es wird erwartet, dass **regelmäßige MVP's** (Minimum viable products) und **Meilensteinberichte** vorgestellt werden, um den Fortschritt des Projekts zu überwachen.

Für den Start und das Testen können vorläufig **offene Quizdatenbanken** genutzt werden wie bspw.:

- Open Tivia DB: https://opentdb.com/
- Cluebase: https://cluebase.readthedocs.io/en/latest/
- Sammlung von Quiz-API's: https://rapidapi.com/search/quiz

### MoSCoW Priorisierung der Anforderungen

#### Must have

- [x] Sie haben eigene kleine Funktionen definiert.
- [x] Sie haben den Programmcode lesbar und verständlich gestaltet.
- [ ] Sie haben das Spiel für die umgesetzten Anforderungen getestet.
- [x] Sie haben einen automatisierten Test implementiert.
- [ ] Sie haben ein Klassendiagramm oder Verteildiagramm ihrer Software erstellt.
- [ ] ~~Das Spiel ist über die Konsole vollständig spielbar~~
- [ ] ~~Es gibt eine Steuerungshilfe, die mit dem Parameter „h“ aufgerufen werden kann.~~
- [x] Am Ende eines jeden Spiels wird eine Rangliste angezeigt.

#### Should have

- [x] Das Spiel ist über eine grafische Oberfläche oder Webseite spielbar.
- [x] Sie haben eine Datenbank erstellt aus der die Quizfragen ausgelesen werden.
- [x] Jeder Spieler hat seinen eigenen Account.
- [x] Eine Rangliste kann jederzeit eingesehen werden.
- Das Programm ist leicht erweiterbar durch:
  - [x] Eine gute Programmstruktur(Module, kleine Funktionen),
  - [x] selbsterklärenden Programmcode, aussagekräftige Namen der Variablen und Funktionen sowie
  - [ ] sinnvolle Kommentare.
- [x] Sie haben drei automatisierte Tests implementiert.

#### Could have

- [ ] Sie haben einen Mehrspielermodus implementiert.
- [x] Über ein separates Backend können die Quizfragen verwaltet (hinzufügen, bearbeiten, löschen) werden.
- [ ] Sie haben ein selbst erdachtes Achievment-System implementiert.
- [ ] Sie haben fünf automatisierte Tests implementiert.
- [x] Sie haben Grundlagen der objektorientierten Programmierung angewendet.
- [x] Sie haben das Spiel so entwickelt, dass es unabhängig vom Betriebsystem läuft. Testen Sie ihr Spiel unter Windows, Mac und Linux.
