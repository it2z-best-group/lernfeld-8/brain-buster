import { test, expect } from "@playwright/test";

test("has proper title", async ({ page }) => {
  await page.goto("/");
  await expect(page).toHaveTitle("Brain Buster");
});

test("greets user", async ({ page }) => {
  await page.goto("/");
  const greeting = await page.locator("div>p").textContent();
  expect(greeting).toEqual(expect.stringMatching(/^Hey .+?!/));
});

test("training answers reveal solution", async ({ page }) => {
  await page.goto("/training");
  const answersLocator = page.locator("body>div>div>div>button");
  await expect(answersLocator).toHaveCount(4);
  await answersLocator.first().click();
  await expect(page.locator("body>div>div>div>button.bg-red-500")).toHaveCount(3);
  await expect(page.locator("body>div>div>div>button.bg-green-500")).toHaveCount(1);
});
