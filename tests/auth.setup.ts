import { test as setup } from "@playwright/test";
import dotenv from "dotenv";
dotenv.config();

const authFile = "playwright/.auth/user.json";

setup("authenticate", async ({ page }) => {
  await page.goto("/api/auth/signin");
  await page.getByLabel("Username").fill("playwright");
  await page.getByLabel("Password").fill(process.env.PLAYWRIGHT_TOKEN!);
  await page.getByRole("button", { name: "Sign in with Credentials" }).click();
  await page.waitForURL("/");
  await page.context().storageState({ path: authFile });
});
