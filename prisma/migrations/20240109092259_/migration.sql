-- CreateTable
CREATE TABLE "MultiplayerMatch" (
    "id" TEXT NOT NULL PRIMARY KEY
);

-- CreateTable
CREATE TABLE "_MultiplayerMatchToUser" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL,
    CONSTRAINT "_MultiplayerMatchToUser_A_fkey" FOREIGN KEY ("A") REFERENCES "MultiplayerMatch" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT "_MultiplayerMatchToUser_B_fkey" FOREIGN KEY ("B") REFERENCES "User" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_AnswerSubmission" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "created" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "questionId" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    "answerId" TEXT NOT NULL,
    "multiplayerMatchId" TEXT,
    CONSTRAINT "AnswerSubmission_questionId_fkey" FOREIGN KEY ("questionId") REFERENCES "Question" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "AnswerSubmission_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "AnswerSubmission_answerId_fkey" FOREIGN KEY ("answerId") REFERENCES "Answer" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "AnswerSubmission_multiplayerMatchId_fkey" FOREIGN KEY ("multiplayerMatchId") REFERENCES "MultiplayerMatch" ("id") ON DELETE SET NULL ON UPDATE CASCADE
);
INSERT INTO "new_AnswerSubmission" ("answerId", "created", "id", "questionId", "userId") SELECT "answerId", "created", "id", "questionId", "userId" FROM "AnswerSubmission";
DROP TABLE "AnswerSubmission";
ALTER TABLE "new_AnswerSubmission" RENAME TO "AnswerSubmission";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;

-- CreateIndex
CREATE UNIQUE INDEX "_MultiplayerMatchToUser_AB_unique" ON "_MultiplayerMatchToUser"("A", "B");

-- CreateIndex
CREATE INDEX "_MultiplayerMatchToUser_B_index" ON "_MultiplayerMatchToUser"("B");
