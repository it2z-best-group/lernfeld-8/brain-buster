/*
  Warnings:

  - You are about to drop the `_MultiplayerMatchToUser` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `firstPlayerId` to the `MultiplayerMatch` table without a default value. This is not possible if the table is not empty.
  - Added the required column `secondPlayerId` to the `MultiplayerMatch` table without a default value. This is not possible if the table is not empty.

*/
-- DropIndex
DROP INDEX "_MultiplayerMatchToUser_B_index";

-- DropIndex
DROP INDEX "_MultiplayerMatchToUser_AB_unique";

-- DropTable
PRAGMA foreign_keys=off;
DROP TABLE "_MultiplayerMatchToUser";
PRAGMA foreign_keys=on;

-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_MultiplayerMatch" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "firstPlayerId" TEXT NOT NULL,
    "secondPlayerId" TEXT NOT NULL,
    CONSTRAINT "MultiplayerMatch_firstPlayerId_fkey" FOREIGN KEY ("firstPlayerId") REFERENCES "User" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "MultiplayerMatch_secondPlayerId_fkey" FOREIGN KEY ("secondPlayerId") REFERENCES "User" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_MultiplayerMatch" ("id") SELECT "id" FROM "MultiplayerMatch";
DROP TABLE "MultiplayerMatch";
ALTER TABLE "new_MultiplayerMatch" RENAME TO "MultiplayerMatch";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
