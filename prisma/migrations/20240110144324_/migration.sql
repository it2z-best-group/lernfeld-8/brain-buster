-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_MultiplayerMatch" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "firstPlayerId" TEXT NOT NULL,
    "secondPlayerId" TEXT NOT NULL,
    "created" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT "MultiplayerMatch_firstPlayerId_fkey" FOREIGN KEY ("firstPlayerId") REFERENCES "User" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "MultiplayerMatch_secondPlayerId_fkey" FOREIGN KEY ("secondPlayerId") REFERENCES "User" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_MultiplayerMatch" ("firstPlayerId", "id", "secondPlayerId") SELECT "firstPlayerId", "id", "secondPlayerId" FROM "MultiplayerMatch";
DROP TABLE "MultiplayerMatch";
ALTER TABLE "new_MultiplayerMatch" RENAME TO "MultiplayerMatch";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
