import { PrismaClient } from "@prisma/client";
import { Prisma } from "@prisma/client";
const prisma = new PrismaClient();
type C =
  | (Prisma.Without<Prisma.CategoryCreateInput, Prisma.CategoryUncheckedCreateInput> &
      Prisma.CategoryUncheckedCreateInput)
  | (Prisma.Without<Prisma.CategoryUncheckedCreateInput, Prisma.CategoryCreateInput> & Prisma.CategoryCreateInput);

async function main() {
  const questions: C[] = [
    {
      text: "Test",
      question: {
        create: [
          {
            text: "What was first?",
            answers: {
              create: [
                { text: "Chicken" },
                { text: "Egg" },
                { text: "neither" },
                { text: "DOOM IS ETERNAL", isRight: true },
              ],
            },
          },
          {
            text: "Was ist die Hauptstadt von Brasilien?",
            answers: {
              create: [
                { text: "Rio de Janeiro" },
                { text: "São Paulo" },
                { text: "Brasília", isRight: true },
                { text: "Salvador" },
              ],
            },
          },
        ],
      },
    },
    {
      text: "Hannover Fragen",
      question: {
        create: [
          {
            text: "Wie viele Einwohner hat Hannover?",
            answers: {
              create: [{ text: "10" }, { text: "100" }, { text: "420" }, { text: "9001", isRight: true }],
            },
          },
        ],
      },
    },
  ];

  await Promise.all([
    ...questions.map((data) => prisma.category.create({ data })),
    prisma.user.create({
      data: { name: "playwright", email: "playwright@example.com", id: "0" },
    }),
  ]);
}
main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
