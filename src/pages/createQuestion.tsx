import { useState } from "react";
import { CreateQuestionData } from "./api/createQuestion";
import axios from "axios";
import { Router, useRouter } from "next/router";

export default function CreateQuestion() {
  const router = useRouter();
  const [questionData, setQuestionData] = useState<CreateQuestionData>({
    question: "",
    category: "general",
    answers: [
      { text: "", isRight: true },
      { text: "", isRight: false },
      { text: "", isRight: false },
      { text: "", isRight: false },
    ],
  });

  return (
    <main className="grid grid-cols-1">
      <label>
        Question:{" "}
        <input
          required
          className="border border-black m-1 p-1 rounded"
          value={questionData.question}
          onChange={(e) => setQuestionData((qD) => ({ ...qD, question: e.target.value }))}
        />
      </label>
      <label>
        Category:{" "}
        <input
          required
          className="border border-black m-1 p-1 rounded"
          value={questionData.category}
          onChange={(e) => setQuestionData((qD) => ({ ...qD, category: e.target.value }))}
        />
      </label>
      <label>
        Answer 1 (correct option):{" "}
        <input
          required
          className="border border-black m-1 p-1 rounded"
          value={questionData.answers[0].text}
          onChange={(e) =>
            setQuestionData((qD) => ({
              ...qD,
              answers: [{ text: e.target.value, isRight: true }, qD.answers[1], qD.answers[2], qD.answers[3]],
            }))
          }
        />
      </label>
      <label>
        Answer 2:{" "}
        <input
          required
          className="border border-black m-1 p-1 rounded"
          value={questionData.answers[1].text}
          onChange={(e) =>
            setQuestionData((qD) => ({
              ...qD,
              answers: [qD.answers[0], { text: e.target.value, isRight: false }, qD.answers[2], qD.answers[3]],
            }))
          }
        />
      </label>
      <label>
        Answer 3:{" "}
        <input
          required
          className="border border-black m-1 p-1 rounded"
          value={questionData.answers[2].text}
          onChange={(e) =>
            setQuestionData((qD) => ({
              ...qD,
              answers: [qD.answers[0], qD.answers[1], { text: e.target.value, isRight: false }, qD.answers[3]],
            }))
          }
        />
      </label>
      <label>
        Answer 4:{" "}
        <input
          required
          className="border border-black m-1 p-1 rounded"
          value={questionData.answers[3].text}
          onChange={(e) =>
            setQuestionData((qD) => ({
              ...qD,
              answers: [qD.answers[0], qD.answers[1], qD.answers[2], { text: e.target.value, isRight: false }],
            }))
          }
        />
      </label>
      <button
        disabled={
          !questionData.question ||
          !questionData.category ||
          questionData.answers.reduce((acc, cur) => !(cur.text || acc), true)
        }
        onClick={() => axios.post("/api/createQuestion", questionData).then(() => router.reload())}
      >
        submit
      </button>
    </main>
  );
}
