import { useSession, signIn, signOut } from "next-auth/react";
import Question from "../components/Question";
import Leaderboard from "@/components/Leaderboard";
import Link from "next/link";

export default function Home() {
  const { data: session } = useSession();
  if (!session) return null;

  return (
    <div className="container mx-auto pt-5">
      <p className="text-xl">
        Hey {session.user?.name}!
        <button className="text-xs rounded m-1 p-1 hover:bg-red-400" onClick={() => signOut()}>
          Sign out
        </button>
      </p>

      <div className="grid grid-cols-2">
        {["profile", "leaderboard", "training", "multiplayer"].map((k) => (
          <Link key={k} className="border border-black rounded p-1 m-1 hover:bg-slate-100 capitalize" href={`/${k}`}>
            {k}
          </Link>
        ))}
      </div>
    </div>
  );
}
