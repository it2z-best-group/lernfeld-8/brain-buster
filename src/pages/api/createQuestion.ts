import { NextApiRequest, NextApiResponse } from "next";
import prisma from "@/prisma";

export type CreateQuestionData = {
  question: string;
  category: string;
  answers: { text: string; isRight: boolean }[];
};

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  const { question, category, answers } = req.body as CreateQuestionData;
  await prisma.question.create({
    data: {
      text: question,
      answers: { create: answers },
      category: { connectOrCreate: { where: { text: category }, create: { text: category } } },
    },
  });
  return res.status(200).json(null);
}
