import { NextApiRequest, NextApiResponse } from "next";
import prisma from "@/prisma";
import type { AnswerSubmission, MultiplayerMatch } from "@prisma/client";

export type CurrentMatchData = {
  answerSubmissions: ({ answer: { isRight: boolean } } & AnswerSubmission)[];
} & MultiplayerMatch;

async function createNewMatch(firstPlayerId: string, secondPlayerId: string) {
  return await prisma.multiplayerMatch.create({
    data: { firstPlayer: { connect: { id: firstPlayerId } }, secondPlayer: { connect: { id: secondPlayerId } } },
    include: { answerSubmissions: { include: { answer: { select: { isRight: true } } } } },
  });
}

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  const { firstPlayerId, secondPlayerId, startNew } = req.query as {
    firstPlayerId: string;
    secondPlayerId: string;
    startNew?: string;
  };

  // find possible running matches for the two users
  const possibleMultiplayerMatchesForUsers = await prisma.multiplayerMatch.findMany({
    where: {
      firstPlayerId: { in: [firstPlayerId, secondPlayerId] },
      secondPlayerId: { in: [firstPlayerId, secondPlayerId] },
    },
    include: { answerSubmissions: { include: { answer: { select: { isRight: true } } } } },
  });

  // start new match or pick running match
  let currentMatch: CurrentMatchData;
  if (!possibleMultiplayerMatchesForUsers.length || startNew) {
    currentMatch = await createNewMatch(firstPlayerId, secondPlayerId);
  } else {
    currentMatch = possibleMultiplayerMatchesForUsers.sort((x, y) => y.created.getTime() - x.created.getTime())[0];
    // } else {
    //   currentMatch = possibleMultiplayerMatchesForUsers[0];
  }

  return res.status(200).json(currentMatch);
}
