import type { NextApiRequest, NextApiResponse } from "next";
import prisma from "@/prisma";
import { Prisma } from "@prisma/client";
import { Answer, Question } from "@prisma/client";
import { shuffle } from "@/utils";
import { DefaultArgs } from "@prisma/client/runtime/library";

export default async function handler(req: NextApiRequest, res: NextApiResponse<Question & { answers: Answer[] }>) {
  //TODO: optimize this
  const category = (req.query.category as string) ?? "all";

  const questions =
    category === "all"
      ? await prisma.question.findMany({
          include: { answers: true },
        })
      : await prisma.question.findMany({
          include: { answers: true },
          where: { categoryId: category },
        });

  // const questions = await prisma.question.findMany(query);
  const question = questions[Math.floor(Math.random() * questions.length)];
  shuffle(question.answers);

  res.status(200).json(question);
}
