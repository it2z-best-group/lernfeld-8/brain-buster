import type { NextApiRequest, NextApiResponse } from "next";
import prisma from "@/prisma";

export default async function handler(_: NextApiRequest, res: NextApiResponse) {
  res.status(200).json(
    (
      await prisma.answerSubmission.findMany({
        where: { answer: { isRight: true } },
        select: { user: { select: { id: true } } },
      })
    )
      // unholy reducer to sum correct answers by users
      .reduce(
        (acc, cur) => {
          const key = cur.user.id!;
          if (acc[key] === undefined) acc[key] = 1;
          else acc[key] += 1;
          return acc;
        },
        {} as { [key: string]: number },
      ),
  );
}
