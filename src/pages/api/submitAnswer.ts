import type { NextApiRequest, NextApiResponse } from "next";
import { getServerSession } from "next-auth";
import { authOptions } from "./auth/[...nextauth]";
import prisma from "@/prisma";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  const session = await getServerSession(req, res, authOptions);
  const { answerId, questionId, multiplayerMatchId } = req.body;
  if (!answerId || !questionId || !session || !session.user?.id) return res.status(500).json({});

  const userId = session.user.id;

  const answerSubmission = await prisma.answerSubmission.create({
    data: { answerId, questionId, userId },
  });

  if (multiplayerMatchId) {
    await prisma.multiplayerMatch.update({
      where: { id: multiplayerMatchId },
      data: { answerSubmissions: { connect: { id: answerSubmission.id } } },
    });
  }

  res.status(200).json({});
}
