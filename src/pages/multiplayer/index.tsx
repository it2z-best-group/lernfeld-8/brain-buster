import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import { getServerSession } from "next-auth";
import prisma from "@/prisma";
import { authOptions } from "../api/auth/[...nextauth]";
import Link from "next/link";

// TODO: Multiplayer
export default function Multiplayer(props: InferGetServerSidePropsType<typeof getServerSideProps>) {
  return (
    <main className="container mx-auto pt-5">
      <ul>
        {props.users.map((userId) => (
          <Link key={userId} href={`/multiplayer/${userId}`}>
            <li>{props.userMapping[userId]}</li>
          </Link>
        ))}
      </ul>
    </main>
  );
}

export const getServerSideProps: GetServerSideProps<{
  userId: string;
  users: string[];
  userMapping: Record<string, string>;
}> = async (ctx) => {
  const session = await getServerSession(ctx.req, ctx.res, authOptions);
  const userId = session?.user.id;
  if (!userId) return { notFound: true };
  const otherUsers = await prisma.user.findMany({ where: { id: { not: userId } }, select: { id: true } });

  const userMapping = (await prisma.user.findMany({ select: { id: true, name: true } })).reduce(
    (acc, cur) => {
      acc[cur.id] = cur.name ?? "Anonymous";
      return acc;
    },
    {} as Record<string, string>,
  );

  return { props: { userId, users: otherUsers.map((u) => u.id), userMapping } };
};
