import Question from "@/components/Question";
import useMultiplayerMatch from "@/hooks/useMultiplayerMatch";
import type { CurrentMatchData } from "@/pages/api/multiplayer/getMatchState";
import prisma from "@/prisma";
import axios from "axios";
import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";

function Scores({ matchState, userMapping }: { matchState: CurrentMatchData; userMapping: Record<string, string> }) {
  const { firstPlayerId, secondPlayerId, answerSubmissions } = matchState;
  const firstPlayerScore = answerSubmissions.filter((e) => e.userId === firstPlayerId && e.answer.isRight).length;
  const secondPlayerScore = answerSubmissions.filter((e) => e.userId === secondPlayerId && e.answer.isRight).length;

  if (firstPlayerScore === secondPlayerScore)
    return (
      <p>
        DRAW! {firstPlayerScore} - {secondPlayerScore}
      </p>
    );
  else if (firstPlayerScore > secondPlayerScore)
    return (
      <p>
        {userMapping[firstPlayerId]} WINS! {firstPlayerScore} - {secondPlayerScore}
      </p>
    );
  else if (firstPlayerScore < secondPlayerScore)
    return (
      <p>
        {userMapping[secondPlayerId]} WINS! {secondPlayerScore} - {firstPlayerScore}
      </p>
    );
}

export default function MultiplayerMatch({ userMapping }: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const router = useRouter();
  const session = useSession();
  const multiplayerMatch = useMultiplayerMatch(session.data?.user.id, router.query.opponentId as string | undefined);

  if (
    !router.query.opponentId ||
    !session.data?.user ||
    typeof router.query.opponentId !== "string" ||
    !multiplayerMatch
  )
    return null;

  return (
    <div className="container mx-auto pt-10">
      <h1 className="font-bold">match page: You vs {userMapping[router.query.opponentId]}</h1>
      {multiplayerMatch.yourTurn ? (
        <Question multiplayerMatchId={multiplayerMatch.matchState.id} refreshMatchState={multiplayerMatch.mutate} />
      ) : multiplayerMatch.matchState.answerSubmissions.length === 8 ? (
        <>
          <Scores matchState={multiplayerMatch.matchState} userMapping={userMapping} />
          <button
            className="border m-1 p-1 rounded border-black hover:bg-slate-200"
            onClick={() =>
              axios
                .get("/api/multiplayer/getMatchState", {
                  params: {
                    firstPlayerId: session.data.user.id,
                    secondPlayerId: router.query.opponentId,
                    startNew: "true",
                  },
                })
                .then(() => multiplayerMatch.mutate())
            }
          >
            new game
          </button>
        </>
      ) : (
        <p>wait for the other player to continue</p>
      )}
      <details>
        <summary>debug</summary>
        <pre className="text-xs">{JSON.stringify(multiplayerMatch, undefined, 2)}</pre>
      </details>
    </div>
  );
}

export const getServerSideProps: GetServerSideProps<{ userMapping: Record<string, string> }> = async () => {
  const userMapping = (await prisma.user.findMany({ select: { id: true, name: true } })).reduce(
    (acc, cur) => {
      acc[cur.id] = cur.name ?? "Anonymous";
      return acc;
    },
    {} as Record<string, string>,
  );

  return { props: { userMapping } };
};
