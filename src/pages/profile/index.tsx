import { GetServerSideProps } from "next";
import { getServerSession } from "next-auth";
import prisma from "@/prisma";
import { authOptions } from "@/pages/api/auth/[...nextauth]";

export default function OwnProfile() {
  return null;
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const session = await getServerSession(ctx.req, ctx.res, authOptions);
  const userId = session?.user.id;
  if (!userId) return { notFound: true };
  const user = await prisma.user.findUnique({ where: { id: userId } });
  if (!user) return { notFound: true };
  return { props: {}, redirect: { destination: `/profile/${user.id}` } };
};
