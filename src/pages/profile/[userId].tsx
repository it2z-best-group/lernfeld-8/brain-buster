import { User } from "@prisma/client";
import { GetServerSideProps } from "next";
import prisma from "@/prisma";

export default function Profile({ user }: { user: User }) {
  return user.name;
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const id = ctx.query.userId;
  if (typeof id !== "string") return { notFound: true };
  const user = await prisma.user.findUnique({
    where: { id },
  });
  if (!user) return { notFound: true };

  return { props: { user } };
};
