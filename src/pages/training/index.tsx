import Question from "@/components/Question";
import prisma from "@/prisma";
import { Category } from "@prisma/client";
import type { GetServerSideProps, InferGetServerSidePropsType } from "next";
import { useState } from "react";

export default function TrainingPage({ categories }: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const [selectedCategory, setSelectedCategory] = useState("all");
  return (
    <div className="container mx-auto pt-5">
      <label>
        <select onChange={(e) => setSelectedCategory(e.target.value)}>
          <option>all</option>
          {categories.map((cat) => (
            <option key={cat.id} value={cat.id}>
              {cat.text}
            </option>
          ))}
        </select>
      </label>
      <Question category={selectedCategory} />
    </div>
  );
}

export const getServerSideProps: GetServerSideProps<{
  categories: Category[];
}> = async (ctx) => {
  const categories = await prisma.category.findMany();
  return { props: { categories } };
};
