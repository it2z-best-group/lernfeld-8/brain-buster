import Leaderboard from "@/components/Leaderboard";
import { GetServerSideProps } from "next";
import prisma from "@/prisma";
import { User } from "@prisma/client";

type Props = { users: Pick<User, "id" | "name">[] };

export default function LeaderboardPage({ users }: Props) {
  return <Leaderboard users={users} />;
}

export const getServerSideProps: GetServerSideProps = async () => {
  const users = await prisma.user.findMany({
    select: { name: true, id: true },
  });
  return { props: { users } };
};
