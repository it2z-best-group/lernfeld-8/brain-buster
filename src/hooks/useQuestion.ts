import { Answer, Question } from "@prisma/client";
import axios from "axios";
import { useEffect, useState } from "react";

export default function useQuestion(category: string) {
  const [question, setQuestion] = useState<Question & { answers: Answer[] }>();
  const [switcher, setSwitcher] = useState(true);

  const next = () => setSwitcher((x) => !x);

  const submit = (answerId: string, multiplayerMatchId: string | undefined = undefined) => {
    if (!question) throw new Error("submit without question");
    axios.post("/api/submitAnswer", { questionId: question.id, answerId, multiplayerMatchId });
  };

  useEffect(() => {
    axios.get("/api/randomQuestion", { params: { category } }).then((r) => setQuestion(r.data));
  }, [switcher, category]);

  return { question, next, submit };
}
