import useSWR from "swr";
import axios from "axios";
import type { CurrentMatchData } from "@/pages/api/multiplayer/getMatchState";

export default function useMultiplayerMatch(currentUserId: string | undefined, secondPlayerId: string | undefined) {
  const { data: matchState, mutate } = useSWR<CurrentMatchData>(
    currentUserId && secondPlayerId ? ["/api/multiplayer/getMatchState", currentUserId, secondPlayerId] : null,
    ([url, firstPlayerId, secondPlayerId]) =>
      axios.get(url, { params: { firstPlayerId, secondPlayerId } }).then((r) => r.data),
  );

  let yourTurn = false;
  if (!matchState) return undefined;

  if (currentUserId === matchState.firstPlayerId) {
    if ([0, 3, 4, 7].includes(matchState.answerSubmissions.length)) {
      yourTurn = true;
    }
  } else {
    if ([1, 2, 5, 6].includes(matchState.answerSubmissions.length)) {
      yourTurn = true;
    }
  }
  return { matchState, yourTurn, mutate };
}
