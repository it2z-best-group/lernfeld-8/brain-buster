import useQuestion from "@/hooks/useQuestion";
import { Answer, Question } from "@prisma/client";
import { useState } from "react";
import { KeyedMutator } from "swr";

export default function Question({
  category = "all",
  multiplayerMatchId = undefined,
  refreshMatchState = async () => null,
}: {
  category?: string;
  multiplayerMatchId?: string;
  refreshMatchState?: KeyedMutator<any>;
}) {
  const { question, next, submit } = useQuestion(category);
  const [isAnswered, setIsAnswered] = useState(false);

  if (!question) return null;

  function handleAnswerSubmission(e: React.MouseEvent<HTMLButtonElement>) {
    const selectedAnswerId = e.currentTarget.id;
    submit(selectedAnswerId, multiplayerMatchId);

    for (const answer of question!.answers) {
      const answerElement = document.getElementById(answer.id);
      if (!answerElement) continue;
      answerElement.classList.add(answer.isRight ? "bg-green-500" : "bg-red-500");
    }
    setIsAnswered(true);
  }

  function handleGotoNextQuestion(question: Question & { answers: Answer[] }) {
    refreshMatchState();

    setIsAnswered(false);
    for (const answer of question.answers) {
      const answerElement = document.getElementById(answer.id);
      if (!answerElement) continue;
      answerElement.classList.remove("bg-green-500", "bg-red-500");
    }
    next();
  }

  return (
    <>
      <div className="grid grid-cols-2 w-max text-center gap-1 border m-1 p-1 border-black rounded">
        <span className="col-span-2 underline text-xl">{question.text}</span>
        {question.answers.map((answer, idx) => (
          <button
            disabled={isAnswered}
            className="border border-black p-1 rounded enabled:hover:bg-slate-100 enabled:hover:shadow max-w-sm"
            id={answer.id}
            key={answer.id}
            onClick={handleAnswerSubmission}
          >
            {String.fromCharCode(65 + idx)}: {answer.text}
          </button>
        ))}
      </div>

      <button
        className="border border-black rounded p-1 m-1 hover:bg-slate-100"
        hidden={!isAnswered}
        onClick={() => handleGotoNextQuestion(question)}
      >
        next
      </button>
    </>
  );
}
