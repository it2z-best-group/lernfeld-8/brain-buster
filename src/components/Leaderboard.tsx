import { User } from "@prisma/client";
import axios from "axios";
import Link from "next/link";
import { useEffect, useState } from "react";

export default function Leaderboard({ users }: { users: Pick<User, "id" | "name">[] }) {
  const [lb, setLb] = useState<{ [key: string]: number }>();
  useEffect(() => {
    axios.get("/api/leaderboard").then((r) => setLb(r.data));
  }, []);

  if (!lb) return null;

  return (
    <ol>
      {Object.entries(lb)
        .sort(([, countA], [, countB]) => countB - countA)
        .map(([userId, count]) => (
          <li className="list-inside list-decimal" key={userId}>
            <Link href={`/profile/${userId}`}>{users.find((user) => user.id === userId)?.name}</Link>{" "}
            <span title="total correct answers">({count})</span>
          </li>
        ))}
    </ol>
  );
}
